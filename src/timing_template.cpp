/*!
 * This is a template code to demonstrate how to measure runtime of part of your code.
 * I'm using the chrono C++ library.
 * @date September 8th, 2020.
 * @author Selan
 */

#include <iostream>
#include <iterator>
#include <chrono>
#include <ctime>
#include <algorithm>
#include <cmath>
#include "searching.cpp"

using namespace sa;

int main( void )
{
    unsigned length = (unsigned) pow(10, 8);
    unsigned max = (unsigned) pow(10, 9);
    int * array = new int[max];
    std::cout<<"len,rbs,bs"<<std::endl;
    for(unsigned k{0};k<max;++k)
        array[k]=k+1;

    while(length<=max){

        //std::cout << "\n\n\n_____________________________________"<<length<<"___________________________________\n";
        //std::cout << "\n>>> STARTING computation that needs timing, please WAIT.... <<< (linear search)\n";
        auto start_ls = std::chrono::steady_clock::now();
        
        //================================================================================
        //for ( unsigned e{0}; e<length; ++e )
        //{
            rbsearch( array, array + length, -1, array + length);
        //}
        //================================================================================
        std::chrono::time_point<std::chrono::steady_clock> end_ls = std::chrono::steady_clock::now();
        /*std::cout << ">>> ENDING computation that needs timing <<<\n";

        //Store the time difference between start and end
        // auto diff = end - start;
        std::chrono::duration<double> diff = end - start;

        // Milliseconds (10^-3)
        std::cout << "\t\t>>> " << std::chrono::duration <double, std::milli> (diff).count()
            << " ms" << std::endl;

        // Nanoseconds (10^-9)
        std::cout << "\t\t>>> " << std::chrono::duration <double, std::nano> (diff).count()
            << " ns" << std::endl;

        // Seconds
        auto diff_sec = std::chrono::duration_cast<std::chrono::seconds>(diff);
        std::cout << "\t\t>>> " << diff_sec.count() << " s" << std::endl;



        std::cout << ">>> STARTING computation that needs timing, please WAIT.... <<< (binary search)\n";*/
        auto start_bs = std::chrono::steady_clock::now();
        
        //================================================================================
        //for ( unsigned e{0}; e<length; ++e )
        //{
            bsearch( array, array + length, -1 );
        //}
        //================================================================================
        std::chrono::time_point<std::chrono::steady_clock> end_bs = std::chrono::steady_clock::now();
        /*std::cout << ">>> ENDING computation that needs timing <<<\n";

        //Store the time difference between start and end
        // auto diff = end - start;
        diff = end - start;

        // Milliseconds (10^-3)
        std::cout << "\t\t>>> " << std::chrono::duration <double, std::milli> (diff).count()
            << " ms" << std::endl;

        // Nanoseconds (10^-9)
        std::cout << "\t\t>>> " << std::chrono::duration <double, std::nano> (diff).count()
            << " ns" << std::endl;

        // Seconds
        diff_sec = std::chrono::duration_cast<std::chrono::seconds>(diff);
        std::cout << "\t\t>>> " << diff_sec.count() << " s" << std::endl;
        */
        auto diff_ls = end_ls - start_ls;
        auto diff_bs = end_bs - start_bs;
        std::cout<<length<<","<<std::chrono::duration<double, std::milli>(diff_ls).count()<<","<<std::chrono::duration<double, std::milli>(diff_bs).count()<<std::endl;
        length += 18000000;
    }
    delete []array;
    return EXIT_SUCCESS;
}
